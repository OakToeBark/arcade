class AddNameToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :name, :string
  end
end

# literally adds a column in the users table called name with the variable type
# of string 
